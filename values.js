function values(testObject){
    //if testObj is not an object
    if((typeof testObject !== 'object') || (Array.isArray(testObject)===true))
        return 'Invalid Argument';

    //for storing the list of all values
    let allValues=[];

    for(const key in testObject){

        allValues.push(testObject[key]);

    }
    return allValues;
}

module.exports=values;