function pairs(obj){
    //if obj is not an object
    if((typeof obj !=='object') || (Array.isArray(obj)===true))
        return 'Invalid Argument';

    //for storing the list of all key value pairs
    let allPairs=[];

    for(const key in obj){

        allPairs.push([key,obj[key]]);

    }
    return allPairs;
}
module.exports=pairs;