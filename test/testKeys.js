const keys=require('../keys');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const testKeys=()=>{

    const test1=keys(testObject);

    const test2=keys({});

    const test3=keys([]);

    const test4=keys(undefined);
    
    console.log(test1,test2,test3,test4);
}
testKeys();