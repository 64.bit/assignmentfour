const pairs=require('../pairs');

const testPairs=()=>{

    const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

    const test1=pairs(testObject);

    const test2=pairs(undefined);

    const test3=pairs({});

    const test4=pairs(11);
    
    console.log(test1,test2,test3,test4);
}
testPairs();