const invert=require('../invert');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const testInvert=()=>{

   const test1=invert(testObject);

   const test2=invert({});

   const test3=invert([]);

   const test4=invert(undefined);
   
   console.log(test1,test2,test3,test4);
}
testInvert();