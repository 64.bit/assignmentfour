const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }

const values=require('../values');

const testValues=()=>{

   const test1=values(testObject);

   const test2=values({});

   const test3=values([]);

   const test4=values('Amir');
   
   console.log(test1,test2,test3,test4)
}

testValues();