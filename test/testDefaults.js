const defaults=require('../defaults');

const testObj={flavor: "chocolate"};

const testDefaults=()=>{
   const test1=defaults(testObj,{flavor: "vanilla", sprinkles: "lots"});

   const test2=defaults(undefined,undefined);

   const test3=defaults([],[]);

   const test4=defaults(testObj,undefined);

   const test5=defaults([],testObj);

   console.log(test1,test2,test3,test4,test5);
}
testDefaults();