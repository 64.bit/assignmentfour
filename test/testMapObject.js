const mapObject = require('../mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const testMapObject = () => {

    const callback = (key, value) => {

        return typeof value === 'string' ? value + key : value * 2;
    }
    const test1=mapObject(testObject,callback);

    const test2=mapObject(undefined,callback);

    const test3=mapObject(77,callback);
    
    console.log(test1,test2,test3);
}
testMapObject();