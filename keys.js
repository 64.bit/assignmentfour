function keys(testObject){
    //if testObj is not an object
    if((typeof testObject !== 'object') || (Array.isArray(testObject)===true))
    return 'Invalid Argument';

    //for storing the list of keys
    let allKeys=[]

    for(const key in testObject){

        allKeys.push(key);

    }
    return allKeys;
}

module.exports=keys;