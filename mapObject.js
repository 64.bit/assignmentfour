function mapObject(obj,cb){
  //if obj is not an object
  if((typeof obj !=='object' )|| (Array.isArray(obj)===true)) 
    return 'Invalid Argument';

  //for storing the modified object
  const emptyObj=obj;

  for(const key in obj){

      emptyObj[key]=cb(key,obj[key]);

  }
  return emptyObj;
}

module.exports=mapObject;