function defaults(obj, defaults) {
    // if obj or defaults is not an object
    if((typeof obj !=='object') || (Array.isArray(obj)===true) || (typeof defaults!=='object') || (Array.isArray(defaults)===true))
     return 'Invalid Argument';

     //for resultant object
    let finalObj = obj;

    for (const key in defaults) {

        if (finalObj[key] === undefined) 
            finalObj[key] = defaults[key];

    }
    return finalObj;
}

module.exports=defaults;