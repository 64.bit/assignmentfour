function invert(obj){
    //if obj is not an object
    if((typeof obj!=='object') || (Array.isArray(obj)===true))
     return 'Invalid Argument';

     // for storing inverted object
    let invertedObject={};

    for(const key in obj){

        invertedObject[obj[key]]=key;

    }
    return invertedObject;
}
module.exports=invert;